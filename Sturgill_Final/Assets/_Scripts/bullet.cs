﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{
    private Transform target;
    public float speed = 70f;
    public GameObject impactEffect;
    public float explosionRadius = 0;
    public int damage = 50;
    
    

    public void Seek(Transform _target)
    {
        target = _target;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    

    

    // Update is called once per frame
    void Update()
    {
       if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        }

        transform.Translate(dir.normalized * distanceThisFrame, Space.World);

        transform.LookAt(target);
    }

    void HitTarget()
    {
        GameObject effectIns = (GameObject)Instantiate(impactEffect, transform.position, transform.rotation);
        Destroy(effectIns, 5f);

        if (explosionRadius > 3.5f)
        {
            Explode();
            Destroy(this.gameObject);
        }
        else
        {
            Damage(target);
            Destroy(this.gameObject);
        }

        
    }

    void Damage (Transform enemy)
    {
        
        Enemy e = enemy.GetComponent<Enemy>();
        
        if (e != null)
        {
            
            e.TakeDamage(damage);
        }

    }

    void Explode ()
    {

        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);
        foreach (Collider collider in colliders)
        {
            if (collider.tag == "Enemy")
            {
                Destroy(this);
                Damage(collider.transform);

            }
        }

    }
}
