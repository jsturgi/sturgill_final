﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Shop : MonoBehaviour
{
    public GameObject ballistaPrefab;
    public GameObject cannonPrefab;
    public GameObject slowBeamPrefab;
    public int ballistaPrice = 100;
    public int cannonPrice = 250;
    public int slowBeamPrice = 400;
    public AudioSource Button;
    
    
    
    BuildManager buildManager;

    
    private void Start()
    {
        buildManager = BuildManager.instance;

        
    }
    public void SelectBallista()
    {

        Button.Play();
        buildManager.SelectTurretToBuild(ballistaPrefab, ballistaPrice);
        

    }

    public void SelectSlowBeam()
    {
        Button.Play();
        buildManager.SelectTurretToBuild(slowBeamPrefab, slowBeamPrice);
    }

    public void SelectCannon()
    {
        Button.Play();
        buildManager.SelectTurretToBuild(cannonPrefab, cannonPrice);

        
    }
}
