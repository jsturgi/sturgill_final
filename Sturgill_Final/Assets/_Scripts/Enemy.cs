﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float health = 100;

    public GameObject deathEffect;

    public float startSpeed = 10f;

    public enemyMovement enemyMovement;

    public float slowPercentage;

    private int enemyDead ;

    public bool enemyDied = false;



    private void Start()
    {
        enemyMovement.speed = startSpeed;
    }

    public void TakeDamage(float amount)
    {
        
        health -= amount;
        
        

        if (health <= 0)
        {
            
            Die();
        }

    }
    public void Slow (float pct)
    {
        enemyMovement.speed = startSpeed * (1f-pct);
    }
    void Die ()
    {
        GameObject effect = (GameObject)Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(effect, 3f);
        Destroy(gameObject);
        PlayerStats.Money += 15;
        //waveSpawner.enemiesAlive--;
        enemyDead++;
        enemyDied = true;
        health = health + (enemyDead % PlayerStats.Rounds);
        enemyMovement.speed = enemyMovement.speed + .1f;
    }
}
