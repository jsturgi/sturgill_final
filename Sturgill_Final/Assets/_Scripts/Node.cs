﻿
using UnityEngine;
using UnityEngine.EventSystems;

public class Node : MonoBehaviour
{
    public Color hoverColor;
    public Vector3 positionOffset;
    private Renderer rend;
    private Color startColor;
    public Color cannotBuildColor;

    [Header("Optional")]
    public GameObject turret;

    BuildManager buildManager;
    private void Start()
    {

        rend = GetComponent<Renderer>();
        startColor = rend.material.color;
        buildManager = BuildManager.instance;
    }

    public Vector3 GetBuildPosition ()
    {
        return transform.position + positionOffset;
    }

    private void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;

        if (!buildManager.CanBuild)
        {
            rend.material.color = cannotBuildColor;
            return;
        }
            

        if (turret != null)
        {
            //buildManager.SelectNode(this);
            rend.material.color = cannotBuildColor;
            return;
        }

        buildManager.BuildTurretOn(this);
    }
    private void OnMouseEnter()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;
        

        if (buildManager.hasMoney)
        {
            rend.material.color = hoverColor;
        }
        else
        {
            rend.material.color = cannotBuildColor;
        }
        
            
        
        
            
        
        
        
        
    }

    private void OnMouseExit()
    {
        rend.material.color = startColor;
    }
}
