﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    public Text roundsText;
    public AudioSource Button;


    private void OnEnable()
    {
        roundsText.text = PlayerStats.Rounds.ToString();
    }

    public void Retry()
    {
        Button.Play();       
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        
    }

    public void Menu()
    {
        Button.Play();
        SceneManager.LoadScene(0);
    }
}
