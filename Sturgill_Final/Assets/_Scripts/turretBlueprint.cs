﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class turretBlueprint
{
    
    [SerializeField]
    public GameObject prefab;
    [SerializeField]
    public int cost; 
}
