﻿
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class waveSpawner : MonoBehaviour
{

    public static int enemiesAlive = 0;

    public Wave[] waves;

    public float countdownTimer = 5f;

    private float countdown = 2f;

    private int waveNumber = 0;

    public Transform spawnPoint;

    public Text waveCountdownText;

    public GameObject enemy;

    public AudioSource waveStart;

    

    

    private void Update()
    {
        if (enemiesAlive > 0)
        {
            return;
        }
        if (countdown <= 0f)
        {
            StartCoroutine(SpawnWave());
            countdown = countdownTimer;
            return;

        }

        countdown -= Time.deltaTime;

        countdown = Mathf.Clamp(countdown, 0f, Mathf.Infinity);
        waveCountdownText.text = string.Format("{0:00.00}", countdown);
    }

    IEnumerator SpawnWave()
    {
        PlayerStats.Rounds++;
        waveNumber++;
        waveStart.Play();
        //Wave wave = waves[waveNumber];

        for (int i = 0; i < waveNumber; i++)
        {
            SpawnEnemy();
            yield return new WaitForSeconds(.25f);
        }

        waveNumber++;
       
    }

    void SpawnEnemy()
    {
        Instantiate(enemy, spawnPoint.position, spawnPoint.rotation);
        //enemiesAlive++;
    }
}
