﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static bool gameIsOver;

    public GameObject gameOverUI;

    public AudioSource BGM;

    public AudioSource gameOverMusic;


    // Update is called once per frame
    private void Start()
    {
        gameIsOver = false;
    }
    void Update()
    {
        
        if (gameIsOver)
        {
            BGM.Stop();
            
            return;
        }
        if (PlayerStats.Lives <= 0)
        {
            gameOver();
        }
    }

    void gameOver()
    {
        gameIsOver = true;
        gameOverUI.SetActive(true);
        gameOverMusic.Play();
    }
}
