﻿
using UnityEngine;
[RequireComponent(typeof(Enemy))]
public class enemyMovement : MonoBehaviour
{
    public bool loseLife;

    [HideInInspector]
    public float speed = 10f;

    private Transform target;

    private int wavepointIndex = 0; // current waypoint enemy is pursuing

    

    private Enemy enemy;
    private void Start()
    {
        enemy = GetComponent<Enemy>();
        target = Waypoints.waypoints[0];
    }

    private void Update()
    {
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

        if (Vector3.Distance(transform.position, target.position) <= 0.2f)
        {
            GetNextWaypoint();
        }

        speed = enemy.startSpeed;
    }

    void GetNextWaypoint()
    {
        if (wavepointIndex >= Waypoints.waypoints.Length - 1)
        {
            EndPoint();
            return;
        }
        wavepointIndex++;
        target = Waypoints.waypoints[wavepointIndex];
    }

    void EndPoint()
    {
        Destroy(gameObject);
        PlayerStats.Lives -= 1;
        waveSpawner.enemiesAlive--;
        loseLife = true;

    }
}
