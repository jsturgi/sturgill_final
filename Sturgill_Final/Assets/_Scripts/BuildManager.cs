﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManager : MonoBehaviour
{
    public static BuildManager instance;

    private void Awake()
    {
        if (instance != null)
        {
           Debug.LogError("More than one build manager in scene");
                return;
      }
        instance = this;
        Debug.Log("SET MANAGER!");

    }

    public GameObject cannonPrefab;
    public GameObject slowBeamPrefab;
    public GameObject ballistaPrefab;
    public bool hasMoney;
    public int turretCost;
    public Node node;
    
    

    
    private GameObject turretToBuild;
    private Node selectedNode;

    

    public bool CanBuild { get { return turretToBuild != null; } }
    
    public void BuildTurretOn (Node node)
    {
        if (PlayerStats.Money > turretCost)
        {
            hasMoney = true;
            PlayerStats.Money -= turretCost;
            print(PlayerStats.Money);
            GameObject turret = (GameObject)Instantiate(turretToBuild, node.GetBuildPosition(), Quaternion.identity); //turretToBuild.prefab, node.GetBuildPosition(), Quaternion.identity));
            node.turret = turret;
            
        }
        else
        {
            hasMoney = false;
            Debug.Log("Can't Build");
            
            
        }
    }

    public void SelectTurretToBuild(GameObject turret, int cost)
    {
        if(cost< PlayerStats.Money)
        {
            turretToBuild = turret;
            print(turretToBuild.name);
            turretCost = cost;
            hasMoney = true;
            //selectedNode = null;
        }
        else
        {
            hasMoney = false;
        }
       
        

            
        
        
    }

    //public void SelectNode(Node node)
 //   {
      //  selectedNode = node;
      //  turretToBuild = null;
   // }
}
