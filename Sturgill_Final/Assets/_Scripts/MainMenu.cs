﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public AudioSource Button;
    public void Play()
    {
        Button.Play();
        SceneManager.LoadScene("Level");
    }

    public void Quit()
    {
        Button.Play();
        Application.Quit();
    }
}
